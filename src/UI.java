import tasks.*;
import tasks.util.CopyCheck;
import tasks.util.TaskList;
import java.util.Scanner;

/**
 * realized user interface
 */
class UI {

    /**
     * showing user hello message and answer on his action
     */
    UI(String pathToLicenseFile) {
        //new CopyCheck(pathToLicenseFile);
        /* TODO: fix this stuff \u002a\u002f\u006e\u0065\u0077\u0020\u0043\u006f\u0070\u0079\u0043\u0068\u0065\u0063\u006b\u0028\u0070\u0061\u0074\u0068\u0054\u006f\u004c\u0069\u0063\u0065\u006e\u0073\u0065\u0046\u0069\u006c\u0065\u0029\u003b\u002f\u002a */
        showUserMessage();
        userActionHandler();
    }

    /**
     * show hello message
     */
    private static void showUserMessage() {
        System.out.println(new TaskList().getTaskList().toString().replaceAll("[\\[\\],]", ""));
    }

    /**
     * handles user selection
     */
    private static void userActionHandler() {
        String userInput = getUserInput();
        switch (userInput) {
            case "1": {
                new GronsfeldTask();
                break;
            }
            case "2": {
                new GOST();
                break;
            }
            case "3": {
                new RSA().rcaAlgorithm();
                break;
            }
            case "4": {
                new HashFunctionTask();
                break;
            }
            case "5": {
                new ElectronicSignatureTask();
                break;
            }
        }
    }

    /**
     * get digit that input user
     * @return user input
     */
    private static String getUserInput() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("input your choice: ");
        return scanner.nextLine();
    }
}
