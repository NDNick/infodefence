package tasks;

import java.math.BigInteger;

/**
 * realize GOST 28147-89 algorithm
 */
public class GOST {

    public GOST()  {
        test();
    }
    //был рассмотрен вариант исполнени
    //TODO realized algorithm

    private void test() {
        long[] L0 = codePhraseToASCII("real");
        long[] R0 = codePhraseToASCII("ized");
        long[] X0 = codePhraseToASCII("был ");
        //System.out.println(getValue(L0).add(getValue(R0)));
        addTwoValues(L0[0],R0[0]);

    }

    private void addTwoValues(long firstValue, long secondValue)    {
        long a = Long.parseLong(String.valueOf(firstValue),2);
        long b = Long.parseLong(String.valueOf(secondValue),2);
        System.out.println(Long.toBinaryString(a+b));
    }

    private BigInteger getValue(long[] array){
        String tmp="";
        BigInteger tst;
        for (int i = 0; i < array.length; i++)  {
            tmp += array[i];
        }
        tst = new BigInteger(tmp);
        return tst;
    }

    private long[] codePhraseToASCII(String phraseToCode)    {
        long[] textBlock = new long[phraseToCode.length()];
        for (int i=0; i < textBlock.length; i++)
            textBlock[i] = Long.parseLong(Long.toString(phraseToCode.charAt(i), 2));
        return textBlock;
    }
}
