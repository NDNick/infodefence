package tasks;

import java.io.DataInputStream;
import java.math.BigInteger;
import java.util.Random;
import java.util.Scanner;

/**
 * realized coding string that input user by RSA algorithm
 */
public class RSA {

    private BigInteger N;
    private BigInteger e;//open key
    private BigInteger d;//private key

    /**
     * find necessary parameters
     */
    public RSA() {
        Random r = new Random();
        int bitLength = 1024;
        BigInteger p = BigInteger.probablePrime(bitLength, r);
        BigInteger q = BigInteger.probablePrime(bitLength, r);
        setN(p.multiply(q));
        BigInteger phi = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
        setE(BigInteger.probablePrime(bitLength / 2, r));

        while (phi.gcd(getE()).compareTo(BigInteger.ONE) > 0 && getE().compareTo(phi) < 0)
            getE().add(BigInteger.ONE);

        setD(getE().modInverse(phi));
    }

    public RSA(BigInteger e, BigInteger d, BigInteger N) {//if value received from user
        this.setE(e);
        this.setD(d);
        this.setN(N);
    }

    /**
     * main method that start process
     */
    public void rcaAlgorithm() {
        RSA rsa = new RSA();
        Scanner scanner = new Scanner(System.in);
        DataInputStream in = new DataInputStream(System.in);
        System.out.println("Enter the plain text:");

        String testString = scanner.nextLine();

        System.out.println("Encrypting String: " + testString);
        System.out.println("String in Bytes: " + bytesToString(testString.getBytes()));

        // encrypt
        byte[] encrypted = rsa.encrypt(testString.getBytes());

        // decrypt
        byte[] decrypted = rsa.decrypt(encrypted);

        //better use utf-8 to see it
        //System.out.println("Encrypting String: " + new String(encrypted));
        System.out.println("Decrypting Bytes: " + bytesToString(decrypted));
        System.out.println("Decrypted String: " + new String(decrypted));
    }

    /**
     * util method which helps to convert byte array to string, and check algorithm output
     *
     * @param encrypted bytes array that represent encrypted string that user input before
     * @return message in string represents
     */
    private static String bytesToString(byte[] encrypted) {
        StringBuilder test = new StringBuilder();
        for (byte b : encrypted)
            test.append(Byte.toString(b));
        return test.toString();
    }

    /**
     * Encrypt message
     *
     * @param message array of bytes that represent a code string
     * @return encrypted message
     */
    public byte[] encrypt(byte[] message) {
        return (new BigInteger(message)).modPow(getE(), getN()).toByteArray();
    }

    /**
     * Decrypt message
     *
     * @param message array of bytes that represent a code string
     * @return decrypted message
     */
    public byte[] decrypt(byte[] message) {
        return (new BigInteger(message)).modPow(getD(), getN()).toByteArray();
    }

    public BigInteger getN() {
        return N;
    }

    public void setN(BigInteger n) {
        N = n;
    }

    public BigInteger getE() {
        return e;
    }

    public void setE(BigInteger e) {
        this.e = e;
    }

    public BigInteger getD() {
        return d;
    }

    public void setD(BigInteger d) {
        this.d = d;
    }
}
