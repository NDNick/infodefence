package tasks;

import java.math.BigInteger;

public class ElectronicSignatureTask {

    private BigInteger e, n, d;
    private BigInteger hashCode = BigInteger.valueOf(95);//get from test

    public ElectronicSignatureTask()    {
        RSA rsa = new RSA();
        setPublicKeys(rsa);
        if (hashCode.equals(checkElectronicSignature()))
            System.out.println("Success!");

    }

    private void setPublicKeys(RSA rsa) {
        e = rsa.getE();
        n = rsa.getN();
        d = rsa.getD();
    }

    private BigInteger countElectronicSignature()  {
        return (new BigInteger(String.valueOf(hashCode)).modPow(d,n));
    }

    private BigInteger checkElectronicSignature() {
        return (new BigInteger(String.valueOf(countElectronicSignature())).modPow(e,n));
    }
}
