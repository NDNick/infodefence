package tasks;

import java.util.Scanner;

public class HashFunctionTask {

    public HashFunctionTask() {
        action();
    }

    private void action() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input text: ");
        String text = scanner.nextLine();
        int test = getCode(text);
        System.out.println("hash code of text with mod 4 function: " + test);
        System.out.println("hash code of text with standard java hashCode(): " + text.hashCode());
    }

    private static int getCode(String string)
    {
        int[] array = new int[string.length()];

        char[] charArray = string.toCharArray();

        for (int i = 0; i<array.length; i++) array[i] = charArray[i];

        int result=0;
        for (int anArray : array) result += anArray / 4;

        return result;
    }
}
