package tasks.util;

import java.util.Random;

public class SimpleNumberGenerator {

    private int generateNumber() {
        return new Random().nextInt(99);
    }

    private boolean checkNumber(int number) {
        if (number < 2)
            return false;
        for (int i = 2; i <= number/2; i++) {
            if (number % i == 0)
                return false;
        }
        return true;
    }

    public int getSimpleNumber() {
        while (true) {
            int number = generateNumber();
            if (checkNumber(number)){
                return number;
            }
        }
    }
}
