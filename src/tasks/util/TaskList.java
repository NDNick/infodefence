package tasks.util;

import java.util.ArrayList;
import java.util.List;

public class TaskList {

    private List<String> taskList = new ArrayList<String>();

    public TaskList() {
        initTaskList();
    }

    private void initTaskList() {
        taskList.add(" 1 - Gronsfeld task\n");
        taskList.add("2 - GOST\n");
        taskList.add("3 - RSA\n");
        taskList.add("4 - Hash finder\n");
        taskList.add("5 - Electronic signature");
    }

    public List<String> getTaskList() {
        return taskList;
    }
}
