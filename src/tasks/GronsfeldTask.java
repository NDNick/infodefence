package tasks;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * realized Gronsfield chipper
 */
public class GronsfeldTask {

    /**
     * get key, that need to crypt message
     * also get message and output to check correct of using algorithm
     */
    public GronsfeldTask() {
        Scanner scanner = new Scanner(System.in);
        int key;
        while (true) {
            System.out.println("Input key: ");
            try {
                key = Math.abs(scanner.nextInt());
                scanner.nextLine();
                break;
            } catch (InputMismatchException e) {
                e.printStackTrace();
            }
        }

        int[] aKey = new int[(int) Math.log10(key) + 1];
        for (int i = aKey.length - 1; i >= 0; i--) {
            aKey[i] = key % 10;
            key = key / 10;
        }
        System.out.println("Input text: ");
        String text = scanner.nextLine();
        char[] achText = text.toCharArray();
        char[] achResult = crypt(achText, aKey, true);

        System.out.println("Text: " + String.valueOf(achText));
        System.out.print("Key: ");
        for (int anAKey : aKey) System.out.print(anAKey);
        System.out.println();
        System.out.println("Encrypt: " + String.valueOf(achResult));

        char[] achDecrypt = crypt(achResult, aKey, false);

        System.out.println("Decrypt: " + String.valueOf(achDecrypt));
    }

    /**
     * getting next letter
     *
     * @param c letter
     * @param i index
     * @return next letter
     */
    private static char getNextLetter(char c, int i) {
        c = (char) (c + i);
        if (c < 'A') c = (char) (c + 'z' - 'A' + 1);
        if (c > 'z') c = (char) (c - 'z' + 'A' - 1);
        return c;
    }

    /**
     * crypt message
     *
     * @param achText text to crypt
     * @param aKey    key that get from user
     * @param encrypt comparator
     * @return message
     */
    private static char[] crypt(char[] achText, int[] aKey, boolean encrypt) {
        char[] result = new char[achText.length];
        int posKey = 0;
        for (int i = 0; i < achText.length; i++) {
            result[i] = getNextLetter(achText[i], encrypt ? aKey[posKey++]
                    : (-1) * aKey[posKey++]);
            if (posKey == aKey.length) {
                posKey = 0;
            }
        }
        return result;
    }
}
